module Types where

type CurrencyPair = {
  bid: Float,
  ask: Float
  }

defaultPair : CurrencyPair
defaultPair = { bid = 0, ask = 0 }

data ChartType = Line | Dots

type ChartConfig = {
  chartType : ChartType,
  width : Int,
  height : Int,
  windowSize: Int,
  name : String
  }
