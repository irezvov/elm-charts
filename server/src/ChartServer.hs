module Main (
    main
) where

import qualified Network.WebSockets as WS
import Data.Aeson
import Data.Aeson.Encode (encode)
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BS
import System.Random
import Control.Applicative ((<$>))
import Control.Concurrent (threadDelay)

data CurrencyPair = CurrencyPair {
        bid :: Float,
        ask :: Float
    } deriving (Show, Eq)

instance ToJSON CurrencyPair where
    toJSON (CurrencyPair bid ask) = object [
            T.pack "bid" .= bid,
            T.pack "ask" .= ask
        ]

instance Random CurrencyPair where
    randomR (CurrencyPair l _, CurrencyPair _ h) gen =
        let (bid, gen') = randomR (l, h) gen
            (ask, gen'') = randomR (bid, h) gen'
        in (CurrencyPair bid ask, gen'')
    random = randomR (CurrencyPair 1 1, CurrencyPair 1 2)

generator :: CurrencyPair -> IO CurrencyPair
generator prev@(CurrencyPair bid ask) = do
    threadDelay $ 1 * 1000 * 1000
    let low = prev { bid = bid - 0.03 }
    let high = prev { ask = ask + 0.03 }
    randomRIO (low, high)

accepter :: WS.ServerApp
accepter pending = do
    conn <- WS.acceptRequest pending
    start <- randomIO
    loop conn start
  where
    loop conn prev = do
        pair <- generator prev
        WS.sendTextData conn $ encode pair
        loop conn pair

main :: IO ()
main = WS.runServer "0.0.0.0" 1488 accepter

