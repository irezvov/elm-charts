module Chart (chart) where

import open Types
import open Graphics.Collage
import open Color
import List

chart : [CurrencyPair] -> ChartConfig -> Element
chart ticks cfg =
  let draw = collage cfg.width cfg.height
      ts = ticks |> List.take cfg.windowSize |> List.reverse      
  in case cfg.chartType of
    Line -> toLine cfg ts |> draw
    Dots -> toDots cfg ts |> draw

type Scale = { low: Float, high: Float }

getScaleY : [CurrencyPair] -> Scale
getScaleY ticks = 
  let min = ticks |> List.map .bid |> List.minimum
      max = ticks |> List.map .ask |> List.maximum
      gap = (max - min) * 0.05
  in { low = min - gap, high = max + gap }
     
worldToViewY : Int -> Scale -> Float -> Int
worldToViewY windowHeight scale y = 
  let ratio = (y - scale.low)/(scale.high - scale.low)
  in round <| toFloat windowHeight * ratio - toFloat windowHeight / 2

toLine : ChartConfig -> [CurrencyPair] -> [Form]
toLine cfg ticks =
  let bids = List.map .bid ticks
      asks = List.map .ask ticks
      getY = toFloat . (worldToViewY cfg.height <| getScaleY ticks)
      getX x = x - toFloat cfg.width / 2
      step = toFloat cfg.width / toFloat cfg.windowSize
      xs = List.map (\n -> toFloat n * step) [0..cfg.windowSize-1]
      toPoint x y = (getX x, getY y)
      askPoints = zipWith toPoint xs asks
      bidPoints = zipWith toPoint xs bids
  in [traced (solid red) bidPoints, traced (solid blue) askPoints]

dotsHelper : ChartConfig -> (Float -> Float) -> (CurrencyPair, Int) -> [Form]
dotsHelper cfg getY (currency, n) =
  let x_step = toFloat cfg.width / toFloat cfg.windowSize
      getX x = x - toFloat cfg.width / 2
      a = getX <| x_step * toFloat n
      radius = 2
  in [move (a, getY currency.bid) (filled red (circle radius)),
      move (a, getY currency.ask) (filled blue (circle radius))]

toDots : ChartConfig -> [CurrencyPair] -> [Form]
toDots cfg ticks =
  let getY = toFloat . (worldToViewY cfg.height <| getScaleY ticks)
  in List.concatMap (dotsHelper cfg getY) <| zip ticks [0..cfg.windowSize-1]
