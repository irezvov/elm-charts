module Data (
  stream,
  events,
  reverseEvents
  ) where

import open Json
import Maybe
import Dict as Dict
import WebSocket as WS
import List

import open Types

toList : Signal a -> Signal [a]
toList = foldp (::) []

stream : Signal CurrencyPair
stream = let toCurrency = join . mapMaybe fromJson . fromString
             addr = "ws://localhost:1488"
             events = toCurrency <~ WS.connect addr (constant "")
         in fromJust <~ keepIf isJust (Just defaultPair) events

reverseEvents : Signal [CurrencyPair]
reverseEvents = toList stream

events : Signal [CurrencyPair]
events = 
  let safeTail l = if length l > 0 then List.tail l else []
  in List.reverse . safeTail <~ reverseEvents

join : Maybe (Maybe a) -> Maybe a
join m = case m of
  Just m' -> m'
  _ -> Nothing

mapMaybe : (a -> b) -> Maybe a -> Maybe b
mapMaybe f m = case m of
  Just a -> Just (f a)
  _ -> Nothing

fromJust : Maybe a -> a
fromJust m = case m of Just a -> a

floatFromJust : Maybe JsonValue -> Float
floatFromJust m = case m of (Just (Number a)) -> a

fromJson : JsonValue -> Maybe CurrencyPair
fromJson json = case json of
  Object dict -> 
    let jsFloat (Number f) = f
        mbBid = Dict.lookup "bid" dict
        mbAsk = Dict.lookup "ask" dict
        fmap = flip mapMaybe
        toPair bid ask = { bid = jsFloat bid, ask = jsFloat ask }
    in join <| fmap mbBid (\bid ->
    fmap mbAsk <| toPair bid)
  _ -> Nothing
