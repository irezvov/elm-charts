module Main where

import Data
import Chart
import List
import open Types
import open Graphics.Input

lineConfig : ChartConfig
lineConfig = {
  chartType = Line,
  width = 600,
  height = 400,
  windowSize = 30,
  name = "lines chart"
  }

dotsConfig : ChartConfig
dotsConfig = {
  chartType = Dots,
  width = 600,
  height = 400,
  windowSize = 100,
  name = "dots chart"
  }

chartConfigurations : [ChartConfig]
chartConfigurations = [
  lineConfig,
  dotsConfig
  ]

getChartItem : ChartConfig -> (String, ChartConfig)
getChartItem cfg = (cfg.name, cfg)

(dropList, selectedConfig) = dropDown (List.map getChartItem chartConfigurations)

showCharts : ChartConfig -> [CurrencyPair] -> Element -> Element
showCharts cfg stream drop = flow down [
  drop,
  Chart.chart stream cfg
  ]

main = showCharts <~ selectedConfig ~ Data.reverseEvents ~ dropList
